import escape from 'escape-html';
import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import * as commentService from './commentService';

export const getPosts = (userId, filter) => {
  const result = postRepository.getPosts(filter, userId);
  return result;
};

export const appendComments = async (userId, post) => {
  const comments = await commentService.getCommentsForPost(userId, post.dataValues.id);
  return {
    ...post.get(),
    comments: comments.map(comment => comment.get())
  };
};

export const getPostById = (userId, id) => postRepository.getPostById(id, userId)
  .then(post => appendComments(userId, post));

export const create = (userId, post) => {
  const newPostBody = escape(post.body);

  return postRepository.create({
    ...post,
    userId,
    body: newPostBody,
    isDeleted: false
  });
};

export const deletePost = async (userId, postId) => {
  const post = await postRepository.getPostById(postId, userId);
  if (!post.dataValues.own) {
    throw Error('Not authorized to delete this post.');
  }

  return postRepository.deleteById(postId);
};

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result)
    ? { prevReaction: reaction }
    : { ...postReactionRepository.getPostReaction(userId, postId), prevReaction: reaction };
};

export const setBody = async (userId, { postId, body }) => {
  const post = await postRepository.getPostById(postId, userId);
  if (!post.dataValues.own) {
    throw Error('Not authorized to change this post.');
  }

  const result = await postRepository.updateById(postId, { body });

  return result;
};
