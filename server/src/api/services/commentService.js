import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId,
  isDeleted: false
});

export const getCommentsForPost = (userId, postId) => commentRepository.getCommentsForPost(postId, userId);

export const getCommentById = (userId, id) => {
  const result = commentRepository.getCommentById(id, userId);
  return result;
};

export const deleteComment = async (userId, commentId) => {
  const comment = await commentRepository.getCommentById(commentId, userId);
  if (!comment.dataValues.own) {
    throw Error('Not authorized to delete this post.');
  }

  return commentRepository.deleteById(commentId);
};

export const setBody = async (userId, { commentId, body }) => {
  const post = await commentRepository.getCommentById(commentId, userId);
  if (!post.dataValues.own) {
    throw Error('Not authorized to change this post.');
  }

  const result = await commentRepository.updateById(commentId, { body });

  return result;
};

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result)
    ? { prevReaction: reaction }
    : { ...commentReactionRepository.getCommentReaction(userId, commentId), prevReaction: reaction };
};
