import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter, sessionUserId) {
    const {
      from: offset,
      count: limit,
      userId,
      onlyLiked,
      onlyOthers
    } = filter;

    const where = { isDeleted: false };
    if (userId) {
      if (onlyOthers) {
        Object.assign(where, {
          userId: {
            [Op.ne]: userId
          }
        });
      } else {
        Object.assign(where, { userId });
      }
    }
    if (onlyLiked) {
      Object.assign(where,
        {
          id: {
            [Op.in]: [
              sequelize.literal(`
                        (SELECT "postId"
                        FROM "postReactions" as "reaction"
                        WHERE "post"."id" = "reaction"."postId"
                        AND "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = true)`)
            ]
          }
        });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isDeleted" = FALSE)`), 'commentCount'],
          [sequelize.literal(`
                        "post"."id" IN (SELECT "postId"
                        FROM "postReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = true)`), 'isLikedByUser'],
          [sequelize.literal(`
                        "post"."id" IN (SELECT "postId"
                        FROM "postReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = false)`), 'isDislikedByUser'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`"post"."userId" = '${sessionUserId}'`), 'own']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id, sessionUserId) {
    return this.model.findOne({
      group: [
        'post.id',
        'user.id',
        'user->image.id',
        'image.id',
        'comments.id'
      ],
      where: { id, isDeleted: false },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isDeleted" = FALSE)`), 'commentCount'],
          [sequelize.literal(`
                        "post"."id" IN (SELECT "postId"
                        FROM "postReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = true)`), 'isLikedByUser'],
          [sequelize.literal(`
                        "post"."id" IN (SELECT "postId"
                        FROM "postReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = false)`), 'isDislikedByUser'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`"post"."userId" = '${sessionUserId}'`), 'own']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: CommentModel,
        attributes: []
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }]
    });
  }

  async deleteById(id) {
    return this.updateById(id, { isDeleted: true });
  }
}

export default new PostRepository(PostModel);
