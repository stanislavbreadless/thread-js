import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';
import sequelize from '../db/connection';

const commentLikeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  getCommentById(id, sessionUserId) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: {
        id,
        isDeleted: false
      },
      attributes: {
        include: [
          [sequelize.literal(`"comment"."userId" = '${sessionUserId}'`), 'own'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(false))), 'dislikeCount'],
          [sequelize.literal(`
                        "comment"."id" IN (SELECT "commentId"
                        FROM "commentReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = true)`), 'isLikedByUser'],
          [sequelize.literal(`
                        "comment"."id" IN (SELECT "commentId"
                        FROM "commentReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = false)`), 'isDislikedByUser']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: [],
        duplicating: false
      }]
    });
  }

  async getCommentsForPost(postId, sessionUserId) {
    return this.model.findAll({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: {
        isDeleted: false,
        postId
      },
      attributes: {
        include: [
          [sequelize.literal(`"comment"."userId" = '${sessionUserId}'`), 'own'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(false))), 'dislikeCount'],
          [sequelize.literal(`
                        "comment"."id" IN (SELECT "commentId"
                        FROM "commentReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = true)`), 'isLikedByUser'],
          [sequelize.literal(`
                        "comment"."id" IN (SELECT "commentId"
                        FROM "commentReactions" as "reaction"
                        WHERE "reaction"."userId" = '${sessionUserId}'
                        AND "reaction"."isLike" = false)`), 'isDislikedByUser']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: [],
        duplicating: false
      }]
    });
  }

  async deleteById(id) {
    return this.updateById(id, { isDeleted: true });
  }
}

export default new CommentRepository(CommentModel);
