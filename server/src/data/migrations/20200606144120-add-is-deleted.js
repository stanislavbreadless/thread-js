export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('posts', 'isDeleted', {
        type: Sequelize.BOOLEAN,
        allowNull: false
      }, { transaction }),
      queryInterface.addColumn('comments', 'isDeleted', {
        type: Sequelize.BOOLEAN,
        allowNull: false
      }, { transaction })
    ])),
  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('posts', 'isDeleted', { transaction }),
      queryInterface.removeColumn('comments', 'isDeleted', { transaction })
    ]))
};
