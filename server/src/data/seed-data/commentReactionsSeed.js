const now = new Date();

export default new Array(30)
  .fill(true)
  .concat(new Array(30).fill(false))
  .map(isLike => ({
    isLike,
    createdAt: now,
    updatedAt: now
  }));
