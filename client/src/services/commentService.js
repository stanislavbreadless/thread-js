import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}`,
    type: 'GET'
  });
  return response.json();
};

export const updateCommentBody = async (commentId, newBody) => {
  const response = await callWebApi({
    endpoint: '/api/comments/body',
    type: 'PUT',
    request: {
      commentId,
      body: newBody
    }
  });
  return response.json();
};

export const deleteComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      commentId,
      isLike: true
    }
  });
  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      commentId,
      isLike: false
    }
  });
  return response.json();
};
