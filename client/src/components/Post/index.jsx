import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import ContentEditable from 'react-contenteditable';
import EditControls from '../EditControls';

import styles from './styles.module.scss';

const Post = (
  {
    post,
    likePost,
    dislikePost,
    toggleExpandedPost,
    sharePost,
    startEditingPost,
    savePostChanges,
    discardPostChanges,
    deletePost,
    isEdited
  }
) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    isLikedByUser,
    isDislikedByUser,
    own
  } = post;

  const [editedBody, setEditedBody] = useState(body);

  if (!isEdited && editedBody !== body) {
    setEditedBody(body);
  }

  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta className={styles.postHead}>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {
            own && (
              <EditControls
                model={post}
                isEdited={isEdited}
                currentText={editedBody}
                startEditing={startEditingPost}
                saveChanges={savePostChanges}
                discardChanges={discardPostChanges}
                deleteItem={deletePost}
              />
            )
          }
        </Card.Meta>
        <Card.Description>
          <ContentEditable
            // Not a XSS vulnerability because of React escaping on front-end side
            // and server-side escaping with escape-html
            html={editedBody}
            disabled={!isEdited}
            onChange={ev => setEditedBody(ev.target.value)}
          />
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn + (isLikedByUser ? ` + ${styles.likedButton}` : '')}
          onClick={() => likePost(id)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn + (isDislikedByUser ? ` ${styles.dislikedButton}` : '')}
          onClick={() => dislikePost(id)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  isEdited: PropTypes.bool.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  discardPostChanges: PropTypes.func.isRequired,
  savePostChanges: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  startEditingPost: PropTypes.func
};

Post.defaultProps = {
  startEditingPost: undefined
};

export default Post;
