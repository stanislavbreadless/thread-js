import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import EditControls from 'src/components/EditControls';
import ContentEditable from 'react-contenteditable';

import styles from './styles.module.scss';

const Comment = (
  {
    comment,
    isEdited,
    startEditingComment,
    saveCommentChanges,
    discardCommentChanges,
    deleteComment,
    likeComment,
    dislikeComment
  }
) => {
  const {
    id,
    body,
    createdAt,
    user,
    own,
    dislikeCount,
    likeCount,
    isLikedByUser,
    isDislikedByUser
  } = comment;

  const [editedBody, setEditedBody] = useState(body);

  if (!isEdited && editedBody !== body) {
    setEditedBody(body);
  }

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <div className={styles.commentHead}>
          <span className="date">
            <CommentUI.Author as="a">
              {user.username}
            </CommentUI.Author>
            <CommentUI.Metadata>
              {moment(createdAt).fromNow()}
            </CommentUI.Metadata>
          </span>
          {
            own && (
              <EditControls
                model={comment}
                isEdited={isEdited}
                currentText={editedBody}
                startEditing={startEditingComment}
                saveChanges={saveCommentChanges}
                discardChanges={discardCommentChanges}
                deleteItem={deleteComment}
              />
            )
          }
        </div>
        <CommentUI.Text>
          <ContentEditable
            // Not a XSS vulnerability because of React escaping on front-end side
            // and server-side escaping with escape-html
            html={editedBody}
            disabled={!isEdited}
            onChange={ev => setEditedBody(ev.target.value)}
          />
        </CommentUI.Text>
        <CommentUI.Actions>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn + (isLikedByUser ? ` ${styles.likedButton}` : '')}
            onClick={() => likeComment(id)}
          >
            <Icon name="thumbs up" />
            {likeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn + (isDislikedByUser ? ` ${styles.dislikedButton}` : '')}
            onClick={() => dislikeComment(id)}
          >
            <Icon name="thumbs down" />
            {dislikeCount}
          </Label>
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  isEdited: PropTypes.bool.isRequired,
  discardCommentChanges: PropTypes.func.isRequired,
  saveCommentChanges: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  startEditingComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
