import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon } from 'semantic-ui-react';

import styles from './styles.module.scss';

const EditControls = (
  {
    model,
    isEdited,
    currentText,
    startEditing,
    saveChanges,
    discardChanges,
    deleteItem
  }
) => {
  const { id } = model;

  if (!isEdited) {
    return (
      <div className={styles.postControls}>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => startEditing(id)}>
          <Icon name="edit" />
        </Label>
        <div className={styles.rightToolBarBtn}>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => deleteItem(id)}
          >
            <Icon name="close" />
          </Label>
        </div>
      </div>
    );
  }

  return (
    <div className={styles.postControls}>
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => saveChanges(currentText)}>
        <Icon name="save outline" />
      </Label>
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => discardChanges()}>
        <Icon name="trash alternate" />
      </Label>
    </div>
  );
};

EditControls.propTypes = {
  model: PropTypes.objectOf(PropTypes.any).isRequired,
  isEdited: PropTypes.bool.isRequired,
  startEditing: PropTypes.func.isRequired,
  saveChanges: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired,
  discardChanges: PropTypes.func.isRequired,
  currentText: PropTypes.string.isRequired
};

export default EditControls;
