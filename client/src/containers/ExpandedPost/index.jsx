import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  startEditingPost,
  discardPostChanges,
  deletePost,
  savePostChanges,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  startEditingComment,
  discardCommentChanges,
  deleteComment,
  saveCommentChanges,
  likeComment,
  dislikeComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  editedPost,
  likePost: like,
  dislikePost: dislike,
  startEditingPost: startEdit,
  discardPostChanges: discard,
  savePostChanges: save,
  toggleExpandedPost: toggle,
  deletePost: remove,
  editedComment,
  addComment: add,
  startEditingComment: startEditComment,
  discardCommentChanges: discardComment,
  deleteComment: removeComment,
  saveCommentChanges: saveComment,
  likeComment: commentLike,
  dislikeComment: commentDislike
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            startEditingPost={startEdit}
            discardPostChanges={discard}
            savePostChanges={save}
            sharePost={sharePost}
            deletePost={remove}
            isEdited={editedPost?.id === post.id}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  startEditingComment={startEditComment}
                  discardCommentChanges={discardComment}
                  deleteComment={removeComment}
                  saveCommentChanges={saveComment}
                  isEdited={editedComment?.id === comment.id}
                  likeComment={commentLike}
                  dislikeComment={commentDislike}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  editedPost: PropTypes.objectOf(PropTypes.any),
  editedComment: PropTypes.objectOf(PropTypes.any),
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  startEditingPost: PropTypes.func.isRequired,
  discardPostChanges: PropTypes.func.isRequired,
  savePostChanges: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  startEditingComment: PropTypes.func.isRequired,
  discardCommentChanges: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  saveCommentChanges: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  editedPost: undefined,
  editedComment: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  editedPost: rootState.posts.editedPost,
  editedComment: rootState.posts.editedComment
});

const actions = {
  likePost,
  dislikePost,
  startEditingPost,
  discardPostChanges,
  savePostChanges,
  toggleExpandedPost,
  addComment,
  deletePost,
  startEditingComment,
  discardCommentChanges,
  deleteComment,
  saveCommentChanges,
  likeComment,
  dislikeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
