import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDITED_POST,
  SET_EDITED_COMMENT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditedPostAction = post => ({
  type: SET_EDITED_POST,
  post
});

const setEditedCommentAction = comment => ({
  type: SET_EDITED_COMMENT,
  comment
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(postId);
  const { posts: { posts, expandedPost } } = getRootState();

  const postIndex = posts.findIndex(post => post.id === id);
  const updatedPosts = [...posts];
  updatedPosts.splice(postIndex, 1);

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(undefined));
  }

  dispatch(setPostsAction(updatedPosts));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const updateParam = (param, diff, itemId) => (
  item => {
    if (item.id !== itemId) {
      return item;
    }

    return {
      ...item,
      [param]: Number(item[param]) + diff
    };
  }
);

const setParam = (param, newValue, itemId) => (
  item => {
    if (item.id !== itemId) {
      return item;
    }

    return {
      ...item,
      [param]: newValue
    };
  }
);

const composeFunctions = (...functions) => argument => functions.reduceRight((arg, fn) => fn(arg), argument);

export const likePost = postId => async (dispatch, getRootState) => {
  const { prevReaction } = await postService.likePost(postId);
  const dislikeDiff = prevReaction?.isLike === false ? -1 : 0;
  const likeDiff = prevReaction?.isLike === true ? -1 : 1;

  const { posts: { posts, expandedPost } } = getRootState();

  const mapLikeCount = updateParam('likeCount', likeDiff, postId);
  const mapDislikeCount = updateParam('dislikeCount', dislikeDiff, postId);
  const mapIsLikedByUser = setParam('isLikedByUser', likeDiff === 1, postId);
  const mapIsDislikedByUser = setParam('isDislikedByUser', false, postId);

  const mapPost = composeFunctions(mapLikeCount, mapDislikeCount, mapIsLikedByUser, mapIsDislikedByUser);

  const updated = posts.map(mapPost);

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapPost(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { prevReaction } = await postService.dislikePost(postId);
  const likeDiff = prevReaction?.isLike === true ? -1 : 0;
  const dislikeDiff = prevReaction?.isLike === false ? -1 : 1;

  const { posts: { posts, expandedPost } } = getRootState();

  const mapLikeCount = updateParam('likeCount', likeDiff, postId);
  const mapDislikeCount = updateParam('dislikeCount', dislikeDiff, postId);
  const mapIsLikedByUser = setParam('isLikedByUser', false, postId);
  const mapIsDislikedByUser = setParam('isDislikedByUser', dislikeDiff === 1, postId);

  const mapPost = composeFunctions(mapLikeCount, mapDislikeCount, mapIsLikedByUser, mapIsDislikedByUser);

  const updated = posts.map(mapPost);

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapPost(expandedPost)));
  }
};

export const startEditingPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditedPostAction(post));
};

export const discardPostChanges = () => dispatch => {
  // Discarding = setting edited post to undefined
  dispatch(setEditedPostAction(undefined));
};

export const savePostChanges = newBody => async (dispatch, getRootState) => {
  const { posts: { editedPost, posts, expandedPost } } = getRootState();
  // Nothing was edited means nothing to save
  if (!editedPost || !editedPost.id) {
    return;
  }

  await postService.updatePostBody(editedPost.id, newBody);

  const mapBody = post => ({
    ...post,
    body: newBody
  });

  const updated = posts.map(post => (post.id !== editedPost.id
    ? post
    : mapBody(post)));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === editedPost.id) {
    dispatch(setExpandedPostAction(mapBody(expandedPost)));
  }
  // After saving stop editing mode
  dispatch(setEditedPostAction(undefined));
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const startEditingComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setEditedCommentAction(comment));
};

export const discardCommentChanges = () => dispatch => {
  // Discarding = setting edited post to undefined
  dispatch(setEditedCommentAction(undefined));
};

export const saveCommentChanges = newBody => async (dispatch, getRootState) => {
  const { posts: { editedComment, posts, expandedPost } } = getRootState();
  // Nothing was edited means nothing to save
  if (!editedComment || !editedComment.id) {
    return;
  }

  const { id } = await commentService.updateCommentBody(editedComment.id, newBody);
  const newComment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id === id ? newComment : comment))
  });

  const postId = { newComment };

  const updated = posts.map(post => (post.id !== postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));
  if (expandedPost) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
  // After saving stop editing mode
  dispatch(setEditedCommentAction(undefined));
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.deleteComment(commentId);
  const { posts: { posts, expandedPost } } = getRootState();

  const mapComment = comments => {
    const newComments = [...(comments || [])];
    newComments.splice(newComments.findIndex(comment => comment.id === commentId), 1);
    return newComments;
  };

  const mapComments = post => ({
    ...post,
    comments: mapComment(post.comments),
    commentCount: Number(post.commentCount) - 1
  });

  const updatedPosts = posts.map(post => (post.id === postId ? mapComments(post) : post));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }

  dispatch(setPostsAction(updatedPosts));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { prevReaction } = await commentService.likeComment(commentId);
  const { postId } = await commentService.getComment(commentId);

  const dislikeDiff = prevReaction?.isLike === false ? -1 : 0;
  const likeDiff = prevReaction?.isLike === true ? -1 : 1;

  const { posts: { posts, expandedPost } } = getRootState();

  const mapLikeCount = updateParam('likeCount', likeDiff, commentId);
  const mapDislikeCount = updateParam('dislikeCount', dislikeDiff, commentId);
  const mapIsLikedByUser = setParam('isLikedByUser', likeDiff === 1, commentId);
  const mapIsDislikedByUser = setParam('isDislikedByUser', false, commentId);
  const mapComment = composeFunctions(mapLikeCount, mapDislikeCount, mapIsLikedByUser, mapIsDislikedByUser);

  const mapComments = post => (post.id !== postId
    ? post
    : { ...post, comments: (post.comments ? post.comments.map(mapComment) : []) });

  const updated = posts.map(mapComments);

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { prevReaction } = await commentService.dislikeComment(commentId);
  const { postId } = await commentService.getComment(commentId);

  const likeDiff = prevReaction?.isLike === true ? -1 : 0;
  const dislikeDiff = prevReaction?.isLike === false ? -1 : 1;

  const { posts: { posts, expandedPost } } = getRootState();

  const mapLikeCount = updateParam('likeCount', likeDiff, commentId);
  const mapDislikeCount = updateParam('dislikeCount', dislikeDiff, commentId);
  const mapIsLikedByUser = setParam('isLikedByUser', false, commentId);
  const mapIsDislikedByUser = setParam('isDislikedByUser', dislikeDiff === 1, commentId);

  const mapComment = composeFunctions(mapLikeCount, mapDislikeCount, mapIsLikedByUser, mapIsDislikedByUser);

  const mapComments = post => (post.id !== postId
    ? post
    : { ...post, comments: (post.comments ? post.comments.map(mapComment) : []) });

  const updated = posts.map(mapComments);

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
